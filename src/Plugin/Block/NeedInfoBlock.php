<?php

namespace Drupal\needs_info\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\group\GroupMembershipLoaderInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'NeedInfoBlock' block.
 *
 * @Block(
 *  id = "need_info_block",
 *  admin_label = @Translation("Need info block"),
 * )
 */
class NeedInfoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;
  /**
   * Drupal\group\GroupMembershipLoaderInterface definition.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $groupMembershipLoader;

  /**
   * Constructs a new NeedInfoBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountProxyInterface $current_user, 
	EntityTypeManagerInterface $entity_type_manager, 
	CurrentRouteMatch $current_route_match, 
	GroupMembershipLoaderInterface $group_membership_loader
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $current_route_match;
    $this->groupMembershipLoader = $group_membership_loader;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('group.membership_loader')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
          ] + parent::defaultConfiguration();
  }


  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
		//$member_only = $route->getRequirement('_group_member') === 'TRUE';
		//$permission = $route->getRequirement('_group_permission');

    $parameters = $this->currentRouteMatch->getParameters();
    // no group parameter in route
  	if (!$parameters->has('group')) {
			return AccessResult::forbidden();
		}
		// group is not a 'real' group
		$group = $parameters->get('group');
    if (!$group instanceof GroupInterface) {
      return AccessResult::forbidden();
    }
		return GroupAccessResult::allowedIfHasGroupPermissions($group, $account, ['create group_node:projectes_i_memories entity']);
    //return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['notes_per_mostrar_a_les_entitats'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notes per mostrar a les entitats'),
      '#description' => $this->t('Notes per mostrar a les entitats'),
      '#default_value' => $this->configuration['notes_per_mostrar_a_les_entitats'],
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['notes_per_mostrar_a_les_entitats'] = $form_state->getValue('notes_per_mostrar_a_les_entitats');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    // disable cache as we need to query everytime a group is loaded
    $build['#cache']['max-age'] = 0;
    $group = $this->currentRouteMatch->getParameters()->get('group');
    // First obtain the nids related to that group to allow filtering the
    // related entity via entityquery.
    $query = $this->entityTypeManager->getStorage('group_content')->getQuery();
    $query->condition('type', 'group_content_type_7b142f6d0e0d3');//'projectes_i_memories');
    $query->condition('gid', $group->id());
    $ids = $query->execute();
    $nids = [];
    $group_contents = \Drupal\group\Entity\GroupContent::loadMultiple($ids);
    foreach($group_contents as $key => $group_content){
      $nids[] = $group_content->entity_id->target_id;
    }

    $memories = 0;
    $projects = 0;
    if (!empty($nids)){
      $query = $this->entityTypeManager->getStorage('node')->getQuery(); 
      // Count current projects
      $condition_start = $query->andConditionGroup();
      $condition_start->condition('field_data', date('c', mktime(0, 0, 0, 9, 1, date("Y")-1)), ">=" );
      $condition_start->condition('field_data', date('c', mktime(0, 0, 0, 9, 1, date("Y"))), "<" );
      $condition_end = $query->andConditionGroup();
      $condition_end->condition('field_data_fi', date('c', mktime(0, 0, 0, 9, 1, date("Y")-1)), ">" );
      $condition_end->condition('field_data_fi', date('c', mktime(0, 0, 0, 9, 1, date("Y"))), "<=" );
      $condition_dates = $query->orConditionGroup();
      $condition_dates->condition($condition_start);
      $condition_dates->condition($condition_end);

      //or end date in the current course
      $query->condition('type', 'projectes_i_memories');
      $query->condition('nid', $nids, 'IN');
      $query->condition('field_tipus_document', 'projecte');
      $query->condition($condition_dates);

      $projects = $query->count()->execute();


      // Count past year memories
      $query = $this->entityTypeManager->getStorage('node')->getQuery();
      /* If only using start date as reference 
      $query->condition('field_data', date('c', mktime(0,0,0,9,1, date("Y")-2)), ">=" );
      $query->condition('field_data', date('c', mktime(0,0,0,9,1, date("Y")-1)), "<" );
       */

      $condition_start = $query->andConditionGroup();
      $condition_start->condition('field_data', date('c', mktime(0, 0, 0, 9, 1, date("Y")-2)), ">=" );
      $condition_start->condition('field_data', date('c', mktime(0, 0, 0, 9, 1, date("Y")-1)), "<" );
      $condition_end = $query->andConditionGroup();
      $condition_end->condition('field_data_fi', date('c', mktime(0, 0, 0, 9, 1, date("Y")-2)), ">" );
      $condition_end->condition('field_data_fi', date('c', mktime(0, 0, 0, 9, 1, date("Y")-1)), "<=" );
      $condition_dates = $query->orConditionGroup();
      $condition_dates->condition($condition_start);
      $condition_dates->condition($condition_end);

      // or end date in the previous course
      $query->condition('nid', $nids, 'IN');
      $query->condition('type','projectes_i_memories');
      $query->condition('field_tipus_document', 'memoria');
      $query->condition($condition_dates);
      $memories = $query->count()->execute();
    }

    if ($memories < 1 or $projects < 1 or !empty($this->configuration['notes_per_mostrar_a_les_entitats'])){
      $build['needs_info_wrapper'] = [
        '#theme' => 'container',
        '#attributes' => [
          'class' => ['needs-info',  'alert', 'alert-warning', 'alert-dismissible'],
          'role' => 'alert',
        ]
      ];
      $build['needs_info_wrapper']['#children']['close'] = [
        '#type' => 'button',
        '#value' => 'x',
        '#attributes' => ['class' => ['needs-info', 'close'], 'data-dismiss' => 'alert', 'aria-label' => "Close" ],
      ];
      $build['needs_info_wrapper']['#children']['needs_info'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => $this->t('Needs info'),
        '#attributes' => ['class' => ['needs-info']],
        '#wrapper_attributes' => ['class' => ['container', 'alert', 'alert-warning', 'alert-dismissible']],
      ];
    }
    //Construct the link to allow create the content
    $url = \Drupal\Core\Url::fromRoute('entity.group_content.create_form', ['group' => $group->id(), 'plugin_id' => 'group_node:projectes_i_memories']);
    $link = \Drupal\Core\Link::fromTextAndUrl($this->t('create such content'), $url)->toString();

    // Prepare the render of the items that are needed.
    if ($memories < 1){
      $build['needs_info_wrapper']['#children']['needs_info']['#items'][] = ['#markup' => $this->t('No memories found in last academic year (%academic_year). Please %create_link.', ['%academic_year' => (date("Y")-2) . "/ " . (date("Y")-1), '%create_link' => $link ] )]; 
    }
    if ($projects < 1){
      $build['needs_info_wrapper']['#children']['needs_info']['#items'][] = ['#markup' => $this->t('No projects found in current academic year (%academic_year). Please %create_link.', ['%academic_year' => date("Y")-1 . "/ " . date("Y"), '%create_link' => $link])];
    }
    if (!empty($this->configuration['notes_per_mostrar_a_les_entitats'])){
      $build['needs_info_wrapper']['#children']['needs_info']['#items'][] = ['#markup' => $this->configuration['notes_per_mostrar_a_les_entitats']];
    }
    //$this->theme($build['needs_info_list']);

    //$build['need_info_block_notes_per_mostrar_a_les_entitats']['#markup'] = '<p>' . $this->configuration['notes_per_mostrar_a_les_entitats'] . 'BLOXK!</p>';

    return $build;
  }

}
