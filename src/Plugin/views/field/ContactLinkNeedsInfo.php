<?php

namespace Drupal\needs_info\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\contact\Plugin\views\field\ContactLink;
use Drupal\views\ResultRow;

/**
 * Defines a field that links to the user contact pagei to request empty info,
 * if access is permitted.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("contact_link_needs_info")
 */
class ContactLinkNeedsInfo extends ContactLink {

 /**
  *    {@inheritdoc}
  */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['prefilled_message'] = ['default' => ""];
    $options['prefilled_subject'] = ['default' => ""];
    return $options;
  }


  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['text']['#title'] = $this->t('Link label');
    $form['text']['#required'] = TRUE;
    $form['text']['#default_value'] = empty($this->options['text']) ? $this->getDefaultLabel() : $this->options['text'];
    $form['prefilled_subject'] = [
      '#title' => $this->t('Prefilled subject'),
      '#description' => $this->t('Prefilled subject.  You may include HTML or Twig. You may enter data from this view as per the "Replacement patterns" below.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['prefilled_subject'],
    ];
    $form['prefilled_message'] = [
      '#title' => $this->t('Prefilled message TODO ADD TWIG'),
      '#description' => $this->t('Prefilled meesage.  You may include HTML or Twig. You may enter data from this view as per the "Replacement patterns" below.'),
      '#type' => 'textarea',
      '#default_value' => $this->options['prefilled_message'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    return Url::fromRoute('entity.user.contact_form', [
      'user' => $this->getEntity($row)->id(),
      'emptydoc' => 1,
      'prefilled_message' => $this->tokenizeValue($this->options['prefilled_message'], $row->index),
      'prefilled_subject' => $this->tokenizeValue($this->options['prefilled_subject'], $row->index)
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row) {
    $entity = $this->getEntity($row);

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['url'] = $this->getUrlInfo($row);

    $title = $this->t('Contact %user to request info', ['%user' => $entity->label()]);
    $this->options['alter']['attributes'] = ['title' => $title];

    if (!empty($this->options['text'])) {
      return $this->options['text'];
    }
    else {
      return $title;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('contact to request info');
  }

}

