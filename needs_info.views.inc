<?php

/**
 * @file
 * Provide views data for contact.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function needs_info_views_data_alter(&$data) {
  $data['users']['contact_needs_info'] = [
    'field' => [
      'title' => t('Contact link needs info'),
      'help' => t('Provide a simple link to the user contact page to request for a concrete info.'),
      'id' => 'contact_link_needs_info',
    ],
  ];
}

