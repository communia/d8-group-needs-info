Aquest mòdul afegirà a les rutes del mòdul *Group* un bloc que dirà als usuaris que puguin crear el contingut "projectes i memòries" si hi ha alguna memòria de l'any anterior pendent i si hi ha algun projecte d'aquest any pendent.
Addicionalment permetrà afegir un missatge configurable a la configuració del block *needs info* que veuran tots els grups.

## Contact to request info

Also it adds a new field to users views table to add link to contact for request information defined in views plugin options.
